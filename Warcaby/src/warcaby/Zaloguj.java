/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public final class Zaloguj extends javax.swing.JFrame {

    String login, passw, hashpassw;
    Boolean isadmin;
    Statement stat;
    ResultSet rs;
    Connection con;
    public Zaloguj() {
        initComponents();
    }
    
    public void connectDB(){
        try{
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Warcaby","exus","Zaq12wsx");
            stat = con.createStatement();
        }
        catch(SQLException err){
            System.out.println(err.getMessage());
        }
    }
    
    public void savedate(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        String date_now = dateFormat.format(date);
        
        RandomAccessFile raf = null;
        
        try {
            raf = new RandomAccessFile("ostatnie_logowanie.txt", "rw");
        } catch (FileNotFoundException e) {
            System.out.println("BŁĄD PRZY OTWIERANIU PLIKU!");
            System.exit(1);
        }
        
        try {
            raf.setLength(0);
            for(int i = 0; i < date_now.length(); i++){
                raf.write((int)date_now.charAt(i));
            }
        } catch (IOException e1) {
            System.out.println("BŁĄD ZAPISU Z PLIKU!");
            System.exit(2);
        }

        try {
          raf.close();
        } catch (IOException e) {
            System.out.println("BŁĄD PRZY ZAMYKANIU PLIKU!");
            System.exit(3); 
        }
    }
    
    public void login(){
        String dblogin = "", dbpassw = "";
        login=tf_login.getText();
        passw=pf_haslo.getText();
        
        if(login.matches("") || passw.matches("")){
            JOptionPane.showMessageDialog(null, "Błędne dane logowania");
            tf_login.setText("");
            pf_haslo.setText("");
        }
        else{
            try {
                hashpassw = MySQL.cryptWithMD5(passw);
                rs = stat.executeQuery("SELECT * FROM users WHERE login='" + login + "' && password='" + hashpassw+ "'");
            } catch (SQLException ex) {
                Logger.getLogger(Zaloguj.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                while (rs.next()) {
                    dblogin = rs.getString("login");
                    dbpassw = rs.getString("password");
                    isadmin = rs.getBoolean("isadmin");
                }
            } catch (SQLException ex) {
                Logger.getLogger(Zaloguj.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (login.equals(dblogin) && hashpassw.equals(dbpassw)) {
                savedate();
                Zalogowano y = new Zalogowano(this);
                y.setVisible(true);
                dispose();
            } else {
                JOptionPane.showMessageDialog(null, "Błędne dane logowania");
                tf_login.setText("");
                pf_haslo.setText("");
                tf_login.requestFocus();
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        tf_login = new javax.swing.JTextField();
        pf_haslo = new javax.swing.JPasswordField();
        zaloguj = new javax.swing.JButton();
        cofnij = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        jLabel4.setText("jLabel4");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Logowanie");
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Logowanie");

        tf_login.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        tf_login.setText("exus");
        tf_login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tf_loginActionPerformed(evt);
            }
        });

        pf_haslo.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        pf_haslo.setText("zaq12wsx");
        pf_haslo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pf_hasloKeyPressed(evt);
            }
        });

        zaloguj.setText("Zaloguj");
        zaloguj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zalogujActionPerformed(evt);
            }
        });

        cofnij.setText("Cofnij");
        cofnij.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cofnijActionPerformed(evt);
            }
        });

        jLabel2.setText("Login");

        jLabel3.setText("Hasło");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(260, 260, 260)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(tf_login, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(pf_haslo, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(zaloguj, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cofnij, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(234, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(jLabel2)
                .addGap(6, 6, 6)
                .addComponent(tf_login, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(jLabel3)
                .addGap(6, 6, 6)
                .addComponent(pf_haslo, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(zaloguj, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(cofnij, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(113, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cofnijActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cofnijActionPerformed
        Main x=new Main();
        x.setVisible(true);
        dispose();
    }//GEN-LAST:event_cofnijActionPerformed

    private void zalogujActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zalogujActionPerformed
        connectDB();
        if(con!=null)
            login();
        else
            JOptionPane.showMessageDialog(null, "Błąd połączenia z bazą danych.");
    }//GEN-LAST:event_zalogujActionPerformed

    private void pf_hasloKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pf_hasloKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            login();
        }
    }//GEN-LAST:event_pf_hasloKeyPressed

    private void tf_loginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tf_loginActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tf_loginActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Zaloguj.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Zaloguj.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Zaloguj.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Zaloguj.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Zaloguj().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cofnij;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPasswordField pf_haslo;
    private javax.swing.JTextField tf_login;
    private javax.swing.JButton zaloguj;
    // End of variables declaration//GEN-END:variables
}
