package warcaby;

import java.io.Serializable;

public class Msg implements Serializable {
    String nick, message;
    int type, playerID, removeX, removeY, moveXfrom, moveYfrom, moveXto, moveYto;
    Boolean good2play;
    
    // Typy
    // 1 - good2play, 2 graczy do gry
    // 2 - playerID, przypisanie
    // 3 - message
    // 4 - ruch pionków
    // 5 - usunięcie zbitego pionka
    // 6 - user remove
    
    Msg(){

    }
    
    Msg(int type, Boolean good2play){
        this.type=type;
        this.good2play=good2play;
    }
    
    Msg(int type, String nick, String message){
        this.type=type;
        this.nick=nick;
        this.message=message;
    }
    
    Msg(int type, int playerID){
        this.type=type;
        this.playerID=playerID;
    }
    
    Msg(int type, int playerID, int moveXfrom, int moveYfrom, int moveXto, int moveYto){
        this.type=type;
        this.playerID=playerID;
        this.moveXfrom=moveXfrom;
        this.moveYfrom=moveYfrom;
        this.moveXto=moveXto;
        this.moveYto=moveYto;
    }
    
    Msg(int type, int removeX, int removeY){
        this.type=type;
        this.removeX=removeX;
        this.removeY=removeY;
    }
    
    public void clear(){
        this.type=-1;
        this.playerID=-1;
        this.nick=null;
        this.message=null;
        this.removeX=-1;
        this.removeY=-1;
        this.moveXfrom=-1;
        this.moveYfrom=-1;
        this.moveXto=-1;
        this.moveXto=-1;
    }
}
