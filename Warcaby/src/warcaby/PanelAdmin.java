/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby;

import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;

public class PanelAdmin extends javax.swing.JFrame {

    PreparedStatement ps;
    ResultSet rs;
    Connection con;
    DefaultListModel model, modelfilter;
    Integer rememberID;
    public PanelAdmin() {
        initComponents();
        connectDB();
        model = new DefaultListModel();
        Panel.setVisible(false);
        loadall();
    }
    
    public void connectDB(){
        try{
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Warcaby","exus","Zaq12wsx");
        }
        catch(SQLException err){
            System.out.println(err.getMessage());
            con=null;
        }
    }
    
    public void editselecteduser(){
        String edituser = (String) list_users.getSelectedValue();
        l_edituser.setText(edituser);
        String query1 = "SELECT * FROM users WHERE login=?";
        try {
            ps = con.prepareStatement(query1);
            ps.setString (1, edituser);
            rs = ps.executeQuery();
            while (rs.next()) {
                rememberID=rs.getInt("ID");
                tf_id.setText(rs.getString("ID"));
                tf_login.setText(rs.getString("login"));
                tf_email.setText(rs.getString("email"));
                tf_won.setText(rs.getString("won"));
                tf_lost.setText(rs.getString("lost"));
                cb_admin.setSelected(rs.getBoolean("isadmin"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(PanelAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
        tf_haslo.setText("");
        Panel.setVisible(true);
    }
    
    public void loadall(){
        model.removeAllElements();
        String query1 = "SELECT * FROM users";
        if(con!=null){
            try {
                ps = con.prepareStatement(query1);
                rs = ps.executeQuery();
                while (rs.next()) {
                    model.addElement(rs.getString("login"));
                }
                list_users.setModel(model);
            } catch (SQLException ex) {
                Logger.getLogger(PanelAdmin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            JOptionPane.showMessageDialog(this, "Błąd połączenia z bazą.");
            dispose();
        }
    }
    
    public void filter(){
        loadall();
        modelfilter=model;
        String filter=tf_search.getText();
        int size = modelfilter.getSize();
        String[] users = new String[size];
        for(int i=0;i<size;i++){
            users[i]=(String)modelfilter.getElementAt(i);
        }
        for (String s : users) {
            if (!s.startsWith(filter)) {
                if (modelfilter.contains(s)) {
                    modelfilter.removeElement(s);
                }
            } else {
                if (!modelfilter.contains(s)) {
                    modelfilter.addElement(s);
                }
            }
        }
        list_users.setModel(modelfilter);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        list_users = new javax.swing.JList<>();
        b_wyjdz = new javax.swing.JButton();
        Panel = new javax.swing.JPanel();
        l_edituser = new javax.swing.JLabel();
        l_id = new javax.swing.JLabel();
        tf_login = new javax.swing.JTextField();
        tf_haslo = new javax.swing.JTextField();
        tf_email = new javax.swing.JTextField();
        tf_won = new javax.swing.JTextField();
        tf_lost = new javax.swing.JTextField();
        cb_admin = new javax.swing.JCheckBox();
        l_login = new javax.swing.JLabel();
        l_haslo = new javax.swing.JLabel();
        l_email = new javax.swing.JLabel();
        l_won = new javax.swing.JLabel();
        l_lost = new javax.swing.JLabel();
        l_admin = new javax.swing.JLabel();
        tf_id = new javax.swing.JTextField();
        b_update = new javax.swing.JButton();
        b_delete = new javax.swing.JButton();
        tf_search = new javax.swing.JTextField();
        l_filter = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Panel Admina");
        setResizable(false);

        list_users.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        list_users.setToolTipText("");
        list_users.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                list_usersMouseClicked(evt);
            }
        });
        list_users.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                list_usersKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(list_users);

        b_wyjdz.setText("Wyjdź");
        b_wyjdz.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_wyjdzActionPerformed(evt);
            }
        });

        l_edituser.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        l_edituser.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        l_edituser.setText("nick");

        l_id.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        l_id.setText("ID");

        tf_login.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        tf_haslo.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        tf_email.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tf_email.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tf_emailActionPerformed(evt);
            }
        });

        tf_won.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        tf_lost.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        cb_admin.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        cb_admin.setMaximumSize(new java.awt.Dimension(40, 40));
        cb_admin.setMinimumSize(new java.awt.Dimension(40, 40));
        cb_admin.setPreferredSize(new java.awt.Dimension(40, 40));

        l_login.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        l_login.setText("Login");

        l_haslo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        l_haslo.setText("Hasło");

        l_email.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        l_email.setText("Email");

        l_won.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        l_won.setText("Wygrane");

        l_lost.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        l_lost.setText("Przegrane");

        l_admin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        l_admin.setText("Admin");

        tf_id.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        b_update.setText("Wyślij");
        b_update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_updateActionPerformed(evt);
            }
        });

        b_delete.setForeground(new java.awt.Color(255, 0, 0));
        b_delete.setText("Usuń użytkownika");
        b_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_deleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelLayout = new javax.swing.GroupLayout(Panel);
        Panel.setLayout(PanelLayout);
        PanelLayout.setHorizontalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addGap(345, 345, 345)
                        .addComponent(b_update, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(103, 103, 103)
                        .addComponent(b_delete, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addGap(324, 324, 324)
                        .addComponent(l_edituser, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(l_id, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tf_id, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(l_login, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE)
                            .addComponent(tf_login))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tf_haslo, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(l_haslo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(l_email, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(PanelLayout.createSequentialGroup()
                                .addComponent(tf_email, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelLayout.createSequentialGroup()
                                .addComponent(tf_won, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(tf_lost, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(31, 31, 31)
                                .addComponent(cb_admin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelLayout.createSequentialGroup()
                                .addComponent(l_won, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(l_lost, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(l_admin, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(89, 89, 89))
        );
        PanelLayout.setVerticalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(l_edituser, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(l_login)
                        .addComponent(l_id)
                        .addComponent(l_haslo)
                        .addComponent(l_email))
                    .addComponent(l_won)
                    .addComponent(l_lost)
                    .addComponent(l_admin))
                .addGap(6, 6, 6)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tf_id, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tf_login, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tf_haslo, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tf_won, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tf_email, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(tf_lost, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_admin, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(64, 64, 64)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(b_delete, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(b_update, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tf_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tf_searchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tf_searchKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tf_searchKeyTyped(evt);
            }
        });

        l_filter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        l_filter.setText("Filtruj:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(54, 54, 54)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(458, 458, 458)
                                .addComponent(b_wyjdz, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(11, 11, 11)
                                .addComponent(l_filter, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(4, 4, 4)
                                .addComponent(tf_search, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 75, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 870, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(62, 62, 62)
                        .addComponent(b_wyjdz, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(l_filter))
                    .addComponent(tf_search, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 75, Short.MAX_VALUE)
                .addComponent(Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(56, 56, 56))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void b_wyjdzActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_wyjdzActionPerformed
        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(PanelAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
        dispose();
    }//GEN-LAST:event_b_wyjdzActionPerformed

    private void b_updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_updateActionPerformed
        String login,haslo,email;
        Integer id,won,lost;
        Boolean admin;
        id=Integer.parseInt(tf_id.getText());
        login=tf_login.getText();
        haslo=tf_haslo.getText();
        email=tf_email.getText();
        won=Integer.parseInt(tf_won.getText());
        lost=Integer.parseInt(tf_lost.getText());
        admin=cb_admin.isSelected();
        String query1 = "UPDATE users SET ID=?, " + " login=?, " + " email=?, " + " won=?, " + " lost=?, " + " isadmin=?" + " WHERE ID=?";
        String query2 = "UPDATE users SET ID=?, " + " login=?, " + " password=?, " + " email=?, " + " won=?, " + " lost=?, " + " isadmin=?" + " WHERE ID=?";
        if(haslo.equals("")){
            try {
                ps = con.prepareStatement(query1);
                ps.setInt (1, id);
                ps.setString (2, login);
                ps.setString (3, email);
                ps.setInt (4, won);
                ps.setInt (5, lost);
                ps.setBoolean (6, admin);
                ps.setInt (7, rememberID);
                ps.executeUpdate();
                JOptionPane.showMessageDialog(null, "Zaktualizowano.");
                Panel.setVisible(false);
            } catch (SQLException ex) {
                Logger.getLogger(PanelAdmin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            String hashpassw = MySQL.cryptWithMD5(haslo);
            try {
                ps = con.prepareStatement(query2);
                ps.setInt (1, id);
                ps.setString (2, login);
                ps.setString (3, hashpassw);
                ps.setString (4, email);
                ps.setInt (5, won);
                ps.setInt (6, lost);
                ps.setBoolean (7, admin);
                ps.setInt (8, rememberID);
                ps.executeUpdate();
                JOptionPane.showMessageDialog(null, "Zaktualizowano.");
                Panel.setVisible(false);
            } catch (SQLException ex) {
                Logger.getLogger(PanelAdmin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        l_edituser.setText("");
    }//GEN-LAST:event_b_updateActionPerformed

    private void list_usersMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_list_usersMouseClicked
        list_users = (JList)evt.getSource();
        if (evt.getClickCount() == 2) {
            editselecteduser();
        }
    }//GEN-LAST:event_list_usersMouseClicked

    private void list_usersKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_list_usersKeyPressed
        //String compare=(String) list_users.getSelectedValue();
        if(evt.getKeyCode() == KeyEvent.VK_ENTER && !((String) list_users.getSelectedValue()).equals(l_edituser.getText())){
            editselecteduser();
        }
    }//GEN-LAST:event_list_usersKeyPressed

    private void b_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_deleteActionPerformed
        if(!rememberID.equals(1)){
            int dialog = JOptionPane.YES_NO_OPTION;
            int confirm = JOptionPane.showConfirmDialog(this, "Czy na pewno chcesz usunąć użytkownika " + l_edituser.getText() + "?", "Usuń użytkownika", dialog);
            if(confirm==0){
                String query1 = "DELETE FROM users WHERE ID=?";
                try {
                    ps = con.prepareStatement(query1);
                    ps.setInt (1, rememberID);
                    ps.executeUpdate();
                    JOptionPane.showMessageDialog(null, "Użytkownik" + l_edituser + " został usunięty.");
                    Panel.setVisible(false);
                    loadall();
                } catch (SQLException ex) {
                    Logger.getLogger(PanelAdmin.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        else{
            JOptionPane.showMessageDialog(null, "Nie możesz usunąć głównego admina.");
        }
    }//GEN-LAST:event_b_deleteActionPerformed

    private void tf_searchKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tf_searchKeyTyped

    }//GEN-LAST:event_tf_searchKeyTyped

    private void tf_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tf_searchKeyPressed

    }//GEN-LAST:event_tf_searchKeyPressed

    private void tf_searchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tf_searchKeyReleased
        filter();
        list_users.setSelectedIndex(0);
    }//GEN-LAST:event_tf_searchKeyReleased

    private void tf_emailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tf_emailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tf_emailActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PanelAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new PanelAdmin().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Panel;
    private javax.swing.JButton b_delete;
    private javax.swing.JButton b_update;
    private javax.swing.JButton b_wyjdz;
    private javax.swing.JCheckBox cb_admin;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel l_admin;
    private javax.swing.JLabel l_edituser;
    private javax.swing.JLabel l_email;
    private javax.swing.JLabel l_filter;
    private javax.swing.JLabel l_haslo;
    private javax.swing.JLabel l_id;
    private javax.swing.JLabel l_login;
    private javax.swing.JLabel l_lost;
    private javax.swing.JLabel l_won;
    private javax.swing.JList<String> list_users;
    private javax.swing.JTextField tf_email;
    private javax.swing.JTextField tf_haslo;
    private javax.swing.JTextField tf_id;
    private javax.swing.JTextField tf_login;
    private javax.swing.JTextField tf_lost;
    private javax.swing.JTextField tf_search;
    private javax.swing.JTextField tf_won;
    // End of variables declaration//GEN-END:variables
}
