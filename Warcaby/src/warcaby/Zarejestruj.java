/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/**
 *
 * @author Adam
 */
public final class Zarejestruj extends javax.swing.JFrame {

    /**
     * Creates new form Zarejestruj
     */
    Register player;
    Statement stat;
    ResultSet rs;
    Connection con;
    boolean login_check=false, haslo_check=false, haslo2_check=false, email_check = false;
    public Zarejestruj() {
        initComponents();
        player = new Register();
        login_info.setVisible(false);
        haslo_info.setVisible(false);
        haslo2_info.setVisible(false);
        email_info.setVisible(false);
        b_register.setEnabled(false);
        login_warning.setIcon(UIManager.getIcon("OptionPane.warningIcon"));
        login_warning.setVisible(false);
        haslo_warning.setIcon(UIManager.getIcon("OptionPane.warningIcon"));
        haslo_warning.setVisible(false);
        haslo2_warning.setIcon(UIManager.getIcon("OptionPane.warningIcon"));
        haslo2_warning.setVisible(false);
        email_warning.setIcon(UIManager.getIcon("OptionPane.warningIcon"));
        email_warning.setVisible(false);
        connectDB();
    }
    
    void check_register(){
        if(login_check==true && haslo_check==true && haslo2_check==true && email_check==true){
            b_register.setEnabled(true);
        }
        else{
            b_register.setEnabled(false);
        }
    }
    
    public void connectDB(){
        try{
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Warcaby","exus","Zaq12wsx");
            //stat = con.prepareStatement();
        }
        catch(SQLException err){
            System.out.println(err.getMessage());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        haslo2_info = new javax.swing.JLabel();
        email_info = new javax.swing.JLabel();
        login_info = new javax.swing.JLabel();
        Zarejestruj_sie = new javax.swing.JLabel();
        haslo_info = new javax.swing.JLabel();
        tf_login = new javax.swing.JTextField();
        pf_haslo = new javax.swing.JPasswordField();
        b_register = new javax.swing.JButton();
        b_cofnij = new javax.swing.JButton();
        Login = new javax.swing.JLabel();
        Hasło = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        tf_email = new javax.swing.JTextField();
        pf_haslo2 = new javax.swing.JPasswordField();
        haslo_warning = new javax.swing.JLabel();
        login_warning = new javax.swing.JLabel();
        haslo2_warning = new javax.swing.JLabel();
        email_warning = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Zarejestruj się");
        setPreferredSize(new java.awt.Dimension(1000, 675));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        haslo2_info.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        haslo2_info.setText("Hasła mają być takie same");
        haslo2_info.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        haslo2_info.setOpaque(true);
        getContentPane().add(haslo2_info, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 280, 170, 30));

        email_info.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        email_info.setText("Zły email! Przykład: jankowalski@domena.pl");
        email_info.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        email_info.setOpaque(true);
        getContentPane().add(email_info, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 350, 300, 30));

        login_info.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        login_info.setText("Login powinien zawierać 3-18 znaków");
        login_info.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        login_info.setOpaque(true);
        getContentPane().add(login_info, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 120, 230, 34));

        Zarejestruj_sie.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        Zarejestruj_sie.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Zarejestruj_sie.setText("Zarejestruj się");
        getContentPane().add(Zarejestruj_sie, new org.netbeans.lib.awtextra.AbsoluteConstraints(436, 55, 184, 68));

        haslo_info.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        haslo_info.setText("5-25 znaków, przynajmniej 1 wielka litera, 1 znak specjalny (@#$%^&+=)");
        haslo_info.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        haslo_info.setOpaque(true);
        getContentPane().add(haslo_info, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 200, 460, 29));

        tf_login.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        tf_login.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tf_loginFocusLost(evt);
            }
        });
        tf_login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tf_loginActionPerformed(evt);
            }
        });
        getContentPane().add(tf_login, new org.netbeans.lib.awtextra.AbsoluteConstraints(436, 159, 184, 50));

        pf_haslo.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        pf_haslo.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                pf_hasloFocusLost(evt);
            }
        });
        pf_haslo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pf_hasloActionPerformed(evt);
            }
        });
        getContentPane().add(pf_haslo, new org.netbeans.lib.awtextra.AbsoluteConstraints(436, 240, 184, 50));

        b_register.setText("Zarejestruj");
        b_register.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_registerActionPerformed(evt);
            }
        });
        getContentPane().add(b_register, new org.netbeans.lib.awtextra.AbsoluteConstraints(437, 507, 183, 50));

        b_cofnij.setText("Cofnij");
        b_cofnij.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_cofnijActionPerformed(evt);
            }
        });
        getContentPane().add(b_cofnij, new org.netbeans.lib.awtextra.AbsoluteConstraints(437, 563, 183, 50));

        Login.setText("Login");
        getContentPane().add(Login, new org.netbeans.lib.awtextra.AbsoluteConstraints(436, 139, 86, -1));

        Hasło.setText("Hasło");
        getContentPane().add(Hasło, new org.netbeans.lib.awtextra.AbsoluteConstraints(436, 220, -1, -1));

        jLabel1.setText("Powtórz hasło");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(436, 301, -1, -1));

        jLabel2.setText("Email");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(436, 377, -1, -1));

        tf_email.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        tf_email.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tf_emailFocusLost(evt);
            }
        });
        tf_email.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tf_emailKeyReleased(evt);
            }
        });
        getContentPane().add(tf_email, new org.netbeans.lib.awtextra.AbsoluteConstraints(436, 397, 184, 50));

        pf_haslo2.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        pf_haslo2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                pf_haslo2FocusLost(evt);
            }
        });
        getContentPane().add(pf_haslo2, new org.netbeans.lib.awtextra.AbsoluteConstraints(436, 321, 184, 50));

        haslo_warning.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                haslo_warningMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                haslo_warningMouseExited(evt);
            }
        });
        getContentPane().add(haslo_warning, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 240, 46, 50));

        login_warning.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                login_warningMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                login_warningMouseExited(evt);
            }
        });
        getContentPane().add(login_warning, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 159, 46, 50));

        haslo2_warning.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                haslo2_warningMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                haslo2_warningMouseExited(evt);
            }
        });
        getContentPane().add(haslo2_warning, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 321, 46, 50));

        email_warning.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                email_warningMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                email_warningMouseExited(evt);
            }
        });
        getContentPane().add(email_warning, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 397, 46, 50));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void b_cofnijActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_cofnijActionPerformed
        Main x = new Main();
        x.setVisible(true);
        dispose();
    }//GEN-LAST:event_b_cofnijActionPerformed

    private void tf_loginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tf_loginActionPerformed

    }//GEN-LAST:event_tf_loginActionPerformed

    private void pf_hasloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pf_hasloActionPerformed

    }//GEN-LAST:event_pf_hasloActionPerformed

    private void b_registerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_registerActionPerformed
        player.login=tf_login.getText();
        player.haslo=pf_haslo.getText();
        player.haslo2=pf_haslo2.getText();
        player.email=tf_email.getText();
        
        String templogin="";
        String query1 = "SELECT * FROM users WHERE login=?";
        
        try {
            PreparedStatement preparedStmt1 = con.prepareStatement(query1);
            preparedStmt1.setString (1, player.login);
            rs = preparedStmt1.executeQuery();
            while (rs.next()) {
                templogin = rs.getString("login");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Zaloguj.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (templogin.equals(player.login)) {
            JOptionPane.showMessageDialog(null, "Podany login jest już zajęty.");
        } else {
            try {
                String hashpassw = MySQL.cryptWithMD5(player.haslo);
                String query2 = "INSERT INTO users (login, password, email, won, lost, isadmin)"
                        + "VALUES (?, ?, ?, ?, ?, ?)";
                PreparedStatement preparedStmt = con.prepareStatement(query2);
                preparedStmt.setString (1, player.login);
                preparedStmt.setString (2, hashpassw);
                preparedStmt.setString (3, player.email);
                preparedStmt.setInt (4, 0);
                preparedStmt.setInt (5, 0);
                preparedStmt.setInt (6, 0);
                preparedStmt.execute();
                JOptionPane.showMessageDialog(null, "Rejestracja przebiegła pomyślnie.");
                con.close();
                Main x = new Main();
                x.setVisible(true);
                dispose();
            } catch (SQLException ex) {
                Logger.getLogger(Zarejestruj.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_b_registerActionPerformed

    private void tf_loginFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tf_loginFocusLost
        player.login=tf_login.getText();
        if(!player.login.matches(player.login_check)){
            login_warning.setVisible(true);
            login_check=false;
        }
        else{
            login_warning.setVisible(false);
            login_check=true;
        }
        check_register();
    }//GEN-LAST:event_tf_loginFocusLost

    private void pf_hasloFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pf_hasloFocusLost
        player.haslo=pf_haslo.getText();
        if(!player.haslo.matches(player.haslo_check)){
            haslo_warning.setVisible(true);
            haslo_check=false;
        }
        else{
            haslo_warning.setVisible(false);
            haslo_check=true;
        }
        check_register();
    }//GEN-LAST:event_pf_hasloFocusLost

    private void pf_haslo2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pf_haslo2FocusLost
        player.haslo2=pf_haslo2.getText();
        if(!player.haslo2.equals(player.haslo) || player.haslo2.equals("")){
            haslo2_warning.setVisible(true);
            haslo2_check=false;
        }
        else{
            haslo2_warning.setVisible(false);
            haslo2_check=true;
        }
        check_register();
    }//GEN-LAST:event_pf_haslo2FocusLost

    private void tf_emailFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tf_emailFocusLost
        player.email=tf_email.getText();
        if(!player.email.matches(player.email_check)){
            email_warning.setVisible(true);
            email_check=false;
        }
        else{
            email_warning.setVisible(false);
            email_check=true;
        }
        check_register();
    }//GEN-LAST:event_tf_emailFocusLost

    private void login_warningMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_login_warningMouseEntered
        login_info.setVisible(true);
    }//GEN-LAST:event_login_warningMouseEntered

    private void login_warningMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_login_warningMouseExited
        login_info.setVisible(false);
    }//GEN-LAST:event_login_warningMouseExited

    private void haslo_warningMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_haslo_warningMouseEntered
        haslo_info.setVisible(true);
    }//GEN-LAST:event_haslo_warningMouseEntered

    private void haslo_warningMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_haslo_warningMouseExited
        haslo_info.setVisible(false);
    }//GEN-LAST:event_haslo_warningMouseExited

    private void haslo2_warningMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_haslo2_warningMouseEntered
        haslo2_info.setVisible(true);
    }//GEN-LAST:event_haslo2_warningMouseEntered

    private void haslo2_warningMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_haslo2_warningMouseExited
        haslo2_info.setVisible(false);
    }//GEN-LAST:event_haslo2_warningMouseExited

    private void email_warningMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_email_warningMouseEntered
        email_info.setVisible(true);
    }//GEN-LAST:event_email_warningMouseEntered

    private void email_warningMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_email_warningMouseExited
        email_info.setVisible(false);
    }//GEN-LAST:event_email_warningMouseExited

    private void tf_emailKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tf_emailKeyReleased
        player.email=tf_email.getText();
        if(!player.email.matches(player.email_check)){
            email_warning.setVisible(true);
            email_check=false;
        }
        else{
            email_warning.setVisible(false);
            email_check=true;
        }
        check_register();
    }//GEN-LAST:event_tf_emailKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Zarejestruj.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Zarejestruj.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Zarejestruj.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Zarejestruj.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Zarejestruj().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Hasło;
    private javax.swing.JLabel Login;
    private javax.swing.JLabel Zarejestruj_sie;
    private javax.swing.JButton b_cofnij;
    private javax.swing.JButton b_register;
    private javax.swing.JLabel email_info;
    private javax.swing.JLabel email_warning;
    private javax.swing.JLabel haslo2_info;
    private javax.swing.JLabel haslo2_warning;
    private javax.swing.JLabel haslo_info;
    private javax.swing.JLabel haslo_warning;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel login_info;
    private javax.swing.JLabel login_warning;
    private javax.swing.JPasswordField pf_haslo;
    private javax.swing.JPasswordField pf_haslo2;
    private javax.swing.JTextField tf_email;
    private javax.swing.JTextField tf_login;
    // End of variables declaration//GEN-END:variables
}
