package warcaby;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Gra extends JPanel implements MouseListener {
    public static int width = 720, height = width;
    public static final int tileSize = width/8;
    public static int[][] baseGameData = new int[8][8]; //tablica do rysowania kafelków, 0 - kafelek nie do gry, 1 - kafelek do grania
    public static int[][] gameData = new int[8][8]; //tablica z danymi o pionkach
    public static final int EMPTY = 0, RED = 1, RED_KING = 2, WHITE = 3, WHITE_KING = 4;
    public static JFrame frame;
    public int currentPlayer = RED;
    public boolean inPlay = false; //czy jest mozliwosc ruchu
    public static int[][] availablePlays = new int[8][8]; //mozliwe (podswietlone) ruchy
    public int storedRow, storedCol; //kopia wspolrzednych
    public boolean isJump = false; //czy jest bicie
    static JTextArea ta_chat;
    JLabel l_wait, l_color2;
    
    public int playerID=-1;
    public static Boolean good2play=false;
    
    String nick, ip;
    int port;
    Socket socket;
    ObjectInputStream in;
    ObjectOutputStream out;

    public static void main(String[] args){
        java.awt.EventQueue.invokeLater(new Runnable(){
            public void run(){
                new Gra().setVisible(true);
            }
        });
    }
    
    public Gra(){
        window(width,height,this);
        initializeBoard();
        repaint();
    }
    
    public Gra(String nick, String ip, int port){
        this.nick=nick;
        this.ip=ip;
        this.port=port;
        window(width,height,this);
        initializeBoard();
        repaint();
        connect();
    }
    
    private void connect(){
        try {
            socket = new Socket(ip, port);
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
            listen();
            send("zostal polaczony");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Błąd poczas połączenia z serwerem.");
        }
    }
    
    private void listen(){
        new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true){
                        try {
                            Msg msg = (Msg) in.readObject();
                            switch(msg.type)
                            {
                                case 1:
                                    good2play=msg.good2play;
                                    l_wait.setVisible(false);
                                    break;
                                case 2:
                                    if(playerID==-1){
                                        playerID=msg.playerID;
                                        if(playerID==1){
                                            l_color2.setForeground(Color.RED);
                                            l_color2.setText("Czerwony");
                                        }
                                        else if(playerID==3){
                                            l_color2.setForeground(Color.WHITE);
                                            l_color2.setText("Biały");
                                        }
                                    }
                                    break;
                                case 3:
                                    ta_chat.append(msg.nick + ": " + msg.message + "\n");
                                    break;
                                case 4:
                                    removePiece(msg.moveXfrom,msg.moveYfrom);
                                    movePiece(msg.playerID,msg.moveXto,msg.moveYto);
                                    swapPlayer();
                                    break;
                                case 5:
                                    removePiece(msg.removeX,msg.removeY);
                                    break;
                                case 6:
                                    ta_chat.append(msg.nick + " sie rozlaczyl \n");
                                    break;
                                default:
                                    break;
                            }
                            msg.clear();
                            frame.repaint();
                        } catch (IOException ex) {
                            System.err.println("Blad w watku Klienta" + ex);
                        } catch (ClassNotFoundException ex) {
                            System.err.println("Blad w watku Klienta");
                        }
                    }
                }
            })
        .start();
    }
    
    private void send(String msg){
        Msg message = new Msg(3, nick, msg);
        try {
            out.writeObject(message);
            out.flush();
        } catch (IOException ex) {
            System.err.println("Blad podczas wysylania");
        }
    }
    
    private void send(Msg msg){
        try {
            out.writeObject(msg);
            out.flush();
        } catch (IOException ex) {
            System.err.println("Blad podczas wysylania");
        }
    }
    
    private void exit(){
        Msg message = new Msg (6, nick, "is offline.");
        try {
            out.writeObject(message);
            out.flush();
        } catch (IOException ex) {
            Logger.getLogger(Gra.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public boolean gameOver(){
        return gameOverInternal(0, 0, 0, 0);
    }

    public boolean gameOverInternal(int col, int row, int red, int white){ //rekursja, sprawdzanie końca gry
        if(gameData[col][row] == RED || gameData[col][row] == RED_KING)
            red += 1;
        if(gameData[col][row] == WHITE || gameData[col][row] == WHITE_KING)
            white += 1;
        if(col == 7 && row == 7) {
            if(red == 0 || white == 0)
                return true;
            else 
                return false;
        }
        if(col == 7) {
            row += 1;
            col = -1;
        }
        return gameOverInternal(col+1, row, red, white);
    }

    public void window(int width, int height, Gra game){ //rysuje okno
        frame = new JFrame();
        frame.setSize(width+16, height+36);
        frame.setLocationRelativeTo(null);
        Insets insets = frame.getInsets();
        int frameLeftBorder = insets.left;
        int frameRightBorder = insets.right;
        int frameTopBorder = insets.top;
        int frameBottomBorder = insets.bottom;
        frame.setPreferredSize(new Dimension(width + frameLeftBorder + frameRightBorder + 400, height + frameBottomBorder + frameTopBorder + 30));
        frame.setMaximumSize(new Dimension(width + frameLeftBorder + frameRightBorder + 400, height + frameBottomBorder + frameTopBorder + 30));
        frame.setMinimumSize(new Dimension(width + frameLeftBorder + frameRightBorder + 400, height + frameBottomBorder + frameTopBorder + 30));
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.addMouseListener(this);
        frame.requestFocus();
        frame.setVisible(true);
        ta_chat = new JTextArea();
        ta_chat.setPreferredSize(new Dimension(300,300));
        ta_chat.setMinimumSize(new Dimension(300,300));
        ta_chat.setBounds(760,300,300,300);
        ta_chat.setVisible(true);
        ta_chat.setEditable(false);
        JTextField tf_chat = new JTextField();
        tf_chat.setPreferredSize(new Dimension(300,50));
        tf_chat.setMinimumSize(new Dimension(300,50));
        tf_chat.setBounds(760,610,300,25);
        
        KeyListener keyListener = new KeyListener() {
            @Override
            public void keyPressed(KeyEvent evt) {
                if(evt.getKeyCode() == KeyEvent.VK_ENTER){
                    evt.consume();
                    if (!tf_chat.getText().equals("")) {
                        String message = tf_chat.getText();
                        send(message);
                        tf_chat.setText("");
                    }
                }
            }
            public void keyReleased(KeyEvent keyEvent){}
            public void keyTyped(KeyEvent keyEvent){}
        };
        
        l_wait = new JLabel("Oczekiwanie na drugiego gracza...");
        l_wait.setBounds(800, 20, 250, 40);
        l_wait.setVisible(true);

        
        JLabel l_color = new JLabel("Twoj kolor:");
        l_color.setBounds(760, 65, 70, 30);
        l_color.setVisible(true);
        
        l_color2 = new JLabel();
        l_color2.setBounds(850, 65, 70, 30);

        tf_chat.addKeyListener(keyListener);
        tf_chat.setVisible(true);
        frame.setDefaultLookAndFeelDecorated(true);
        frame.pack();
        frame.add(l_wait);
        frame.add(l_color);
        frame.add(l_color2);
        frame.add(tf_chat);
        frame.add(ta_chat);
        frame.add(game);
    }

    public void initializeBoard(){ //zapisuje początkowe pozycje pionków
        for(int col=0; col < 8; col+=2){
            gameData[col][5] = RED;
            gameData[col][7] = RED;
        }
        for(int col=1; col < 8; col+=2)
            gameData[col][6] = RED;
        for(int col=1; col < 8; col+=2){
            gameData[col][0] = WHITE;
            gameData[col][2] = WHITE;
        }	
        for(int col=0; col < 8; col+=2)
            gameData[col][1] = WHITE;
    }

    public static void drawPiece(int col, int row, Graphics g, Color color){ //rysuje pionki
        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setColor(color);
        g.fillOval((col*tileSize)+2, (row*tileSize)+2, tileSize-4, tileSize-4);
    }

    public void paint(Graphics g){ // rysuje planszę z pionkami
        for(int row = 0; row < 8; row++){
            for(int col = 0; col < 8; col++){
                if((row%2 == 0 && col%2 == 0) || (row%2 != 0 && col%2 != 0)){ //warunek do rysowania kafelków planszy
                    baseGameData[col][row] = 0;
                    g.setColor(Color.gray.brighter());
                    g.fillRect(col*tileSize, row*tileSize, tileSize, tileSize);
                }
                else{
                    baseGameData[col][row] = 1;
                    g.setColor(Color.darkGray);
                    g.fillRect(col*tileSize, row*tileSize, tileSize, tileSize);
                }
                if(checkTeamPiece(col, row) ==  true){			
                    g.setColor(Color.darkGray.darker());
                    g.fillRect(col*tileSize, row*tileSize, tileSize, tileSize);
                }
                if(availablePlays[col][row] == 1){
                    g.setColor(Color.cyan.brighter());
                    g.fillRect(col*tileSize, row*tileSize, tileSize, tileSize);
                }
                if(gameData[col][row] == WHITE)
                    drawPiece(col, row, g, Color.white);
                else if(gameData[col][row] == WHITE_KING){
                    drawPiece(col, row, g, Color.gray.brighter());
                }
                else if(gameData[col][row] == RED)
                    drawPiece(col, row, g, Color.red);
                else if(gameData[col][row] == RED_KING){
                    drawPiece(col, row, g, Color.red.darker().darker());
                }
            }
        }
        if(gameOver() == true)
            gameOverDisplay(g);
    }
    
    public int checkWinner(){ 
        for(int row=0; row<8; row++){
            for(int col=0; col<8; col++){
                if(gameData[col][row]==RED || gameData[col][row]==RED_KING)
                    return 1;
                else if(gameData[col][row]==WHITE || gameData[col][row]==WHITE_KING)
                    return 3;
            }
        }
        return 1;
    }

    public void gameOverDisplay(Graphics g) { //Napis na koniec gry
        String message;
        if(checkWinner()==1)
            message="Gracz czerwony wygrał!";
        else
            message="Gracz biały wygrał!";
        Font font = new Font("Arial", Font.BOLD, 20);
        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (width-80) / 2, width / 2);
    }

    public void resetPlay(){ //usuwa pokazywanie możliwych ruchów (po udanym ruchu lub po złym kliknięciu)
        storedCol = 0;
        storedRow = 0;
        inPlay = false;
        isJump = false;
        for(int row = 0; row < 8; row++){
            for(int col = 0; col < 8; col++){
                availablePlays[col][row] = 0;
            }
        }
        repaint();
    }

    @Override
    public void mousePressed(java.awt.event.MouseEvent evt) { 
        frame.repaint();
        if(playerID==currentPlayer && good2play==true){
            int col = (evt.getX()-8) / tileSize; // 8 - dlugosc lewej ramki
            int row = (evt.getY()-30) / tileSize; // 30 - dlugosc gornej ramki
            if((evt.getX()-8)<720 && (evt.getY()-30)<720) {
                if(inPlay == false && gameData[col][row] != 0 || inPlay == true && checkTeamPiece(col, row) == true){
                    resetPlay();
                    storedCol = col; //zapis wspolrzenych
                    storedRow = row; 
                    getAvailablePlays(col, row);
                }
                else if(inPlay == true && availablePlays[col][row] == 1){
                    makeMove(col, row, storedCol, storedRow);
                }
                else if(inPlay == true && availablePlays[col][row] == 0){
                    resetPlay();
                }
            }
        }
    }

    public void swapPlayer(){ //zmienia graczy po ruchu
        if(currentPlayer == RED)
            currentPlayer = WHITE;
        else
            currentPlayer = RED;
    }

    public void makeMove(int col, int row, int storedCol, int storedRow){ //wykonuje ruch pionkiem
        int x = gameData[storedCol][storedRow]; //change the piece to new tile
        Msg msg = new Msg(4,x,storedCol,storedRow,col,row);
        send(msg);
        if(isJump == true){
            removePiece(col, row, storedCol, storedRow);
        }
        resetPlay();
    }
    
    public void movePiece(int playerID, int col, int row){
        if(playerID==RED && row==0)
            gameData[col][row]=playerID+1;
        else if(playerID==WHITE && row==7)
            gameData[col][row]=playerID+1;
        else
            gameData[col][row]=playerID;
    }

    public void removePiece(int col, int row, int storedCol, int storedRow){ //usuwa pionek na podstawie współrzędnych pionka przeciwnika oraz współrzędnych pionka po biciu
        int pieceRow = -1;
        int pieceCol = -1;
        if(col > storedCol && row > storedRow){
            pieceRow = row-1;
            pieceCol = col-1;
        }
        if(col > storedCol && row < storedRow){
            pieceRow = row+1;
            pieceCol = col-1;
        }
        if(col < storedCol && row > storedRow){
            pieceRow = row-1;
            pieceCol = col+1;
        }
        if(col < storedCol && row < storedRow){
            pieceRow = row+1;
            pieceCol = col+1;
        }
        Msg msg = new Msg(5, pieceCol, pieceRow);
        send(msg);
        //Msg msg2 = new Msg();
        //send(msg2);
    }
    
    public void removePiece(int col, int row){
        gameData[col][row]=EMPTY;
    }

    public void getAvailablePlays(int col, int row){ //sprawdza wszystkie możliwe ruchy danego pionka
        inPlay = true;
        if((checkTeamPiece(col, row) == true)){ //sprawdza czy pionek należy do gracza
            if(gameData[col][row] == RED){  //czerwony może iść tylko do góry
                getUp(col, row);
            }
            if(gameData[col][row] == WHITE){ //biały do dołu
                getDown(col, row);
            }
            if(gameData[col][row] == RED_KING || gameData[col][row] == WHITE_KING){ //król do góry i do dołu
                getUp(col, row);
                getDown(col, row);
            }
        repaint();
        }
    }

    public void getUp(int col, int row){ //możliwość przesunięcia pionka do góry, jak row=0 to koniec planszy
        int rowUp = row-1;
        if(col == 0 && row != 0){ //pionek na lewej krawedzi
            for(int i = col; i < col+2; i++){ //sprawdza w prawo
                if(gameData[col][row] != 0 && gameData[i][rowUp] != 0){
                    if(canJump(col, row, i, rowUp) == true){
                        int jumpCol = getJumpPosition(col, row, i, rowUp)[0];
                        int jumpRow = getJumpPosition(col, row, i, rowUp)[1];
                        availablePlays[jumpCol][jumpRow] = 1;
                    }
                }
                else if(baseGameData[i][rowUp] == 1 && gameData[i][rowUp] == 0)
                    availablePlays[i][rowUp] = 1;
            }
        }
        else if(col == 7 && row != 0){ //pionek na prawej krawdzi
            if(gameData[col][row] != 0 && gameData[col-1][rowUp] != 0){
                if(canJump(col, row, col-1, rowUp) == true){
                    int jumpCol = getJumpPosition(col, row, col-1, rowUp)[0];
                    int jumpRow = getJumpPosition(col, row, col-1, rowUp)[1];
                    availablePlays[jumpCol][jumpRow] = 1;
                }
            }
            else if(baseGameData[col-1][rowUp] == 1 && gameData[col-1][rowUp] == 0)
                availablePlays[col-1][rowUp] = 1;
        }
        else if(col != 7 && col != 0 && row != 0){ //pionek nie na krawedzi, sprawdza w lewo i prawo
            for(int i = col-1; i <= col+1; i++){
                if(gameData[col][row] != 0 && gameData[i][rowUp] != 0){
                    if(canJump(col, row, i, rowUp) == true){
                        int jumpCol = getJumpPosition(col, row, i, rowUp)[0];
                        int jumpRow = getJumpPosition(col, row, i, rowUp)[1];
                        availablePlays[jumpCol][jumpRow] = 1;
                    }
                }
                else if(baseGameData[i][rowUp] == 1 && gameData[i][rowUp] == 0)
                    availablePlays[i][rowUp] = 1;
            }
        }
    }

    public void getDown(int col, int row){ //możliwość przesunięcia pionka do dołu
        int rowDown = row+1;
        if(col == 0 && row != 7){
            if(gameData[col][row] != 0 && gameData[col+1][rowDown] != 0){
                if(canJump(col, row, col+1, rowDown) == true){
                    int jumpCol = getJumpPosition(col, row, col+1, rowDown)[0];
                    int jumpRow = getJumpPosition(col, row, col+1, rowDown)[1];
                    availablePlays[jumpCol][jumpRow] = 1;
                }
            }
            else if(baseGameData[col+1][rowDown] == 1 && gameData[col+1][rowDown] == 0)
                    availablePlays[col+1][rowDown] = 1;
        }
        else if(col == 7 && row != 7){
            if(gameData[col][row] != 0 && gameData[col-1][rowDown] != 0){
                if(canJump(col, row, col-1, rowDown) == true){
                    int jumpCol = getJumpPosition(col, row, col-1, rowDown)[0];
                    int jumpRow = getJumpPosition(col, row, col-1, rowDown)[1];
                    availablePlays[jumpCol][jumpRow] = 1;
                }
            }
            else if(baseGameData[col-1][rowDown] == 1 && gameData[col-1][rowDown] == 0)
                availablePlays[col-1][rowDown] = 1;
        }
        else if(col != 7 && col != 0 && row != 7){
            for(int i = col-1; i <= col+1; i++){
                if(gameData[col][row] != 0 && gameData[i][rowDown] != 0){
                    if(canJump(col, row, i, rowDown) == true){
                        int jumpCol = getJumpPosition(col, row, i, rowDown)[0];
                        int jumpRow = getJumpPosition(col, row, i, rowDown)[1];
                        availablePlays[jumpCol][jumpRow] = 1;
                    }
                }
                else if(baseGameData[i][rowDown] == 1 && gameData[i][rowDown] == 0)
                    availablePlays[i][rowDown] = 1;
            }
        }
    }

    public boolean checkTeamPiece(int col, int row){ //sprawdza czy pionek należy do gracza
        if(currentPlayer == RED && (gameData[col][row] == RED || gameData[col][row] == RED_KING))
            return true;
        if(currentPlayer == WHITE && (gameData[col][row] == WHITE || gameData[col][row] == WHITE_KING))
            return true;
        else
            return false;
    }

    public boolean isLegalPosition(int col, int row){
        if(row < 0 || row >= 8 || col < 0 || col >= 8)
            return false;
        else 
            return true;
    }

    public boolean canJump(int col, int row, int opponentCol, int opponentRow){ //sprawdza możliwość bicia
        if(((gameData[col][row] == WHITE || gameData[col][row] == WHITE_KING) && (gameData[opponentCol][opponentRow] == RED || gameData[opponentCol][opponentRow] == RED_KING)) || (gameData[col][row] == RED || gameData[col][row] == RED_KING) && (gameData[opponentCol][opponentRow] == WHITE || gameData[opponentCol][opponentRow] == WHITE_KING)){ //sprawdza czy przeciwnik ma inny kolor pionka
            if(opponentCol == 0 || opponentCol == 7 || opponentRow == 0 || opponentRow == 7)
                return false;
            int[] toData = getJumpPosition(col, row, opponentCol, opponentRow);
            if(isLegalPosition(toData[0],toData[1]) == false) //sprawdza możliwość ruchu
                return false;
            if(gameData[toData[0]][toData[1]] == 0){
                isJump = true;
                return true;
            }
        }
        return false;
    }

    public int[] getJumpPosition(int col, int row, int opponentCol, int opponentRow){ //sprawdza pozycje w którą pionek się przesunie po biciu
        if(col > opponentCol && row > opponentRow && gameData[col-2][row-2] == 0)
            return new int[] {col-2, row-2};
        else if(col > opponentCol && row < opponentRow && gameData[col-2][row+2] == 0)
            return new int[] {col-2, row+2};
        else if(col < opponentCol && row > opponentRow && gameData[col+2][row-2] == 0)
            return new int[] {col+2, row-2};
        else
            return new int[] {col+2, row+2};
    }

    // Methods that must be included for some reason? WHY
    public void mouseClicked(MouseEvent e) {}
    public void mouseReleased(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}
    public void actionPerformed(ActionEvent e) {}
}