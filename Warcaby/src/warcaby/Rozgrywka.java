package warcaby;

import java.net.*;
import java.io.*;
import java.util.*;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;

public class Rozgrywka extends javax.swing.JFrame
{
    String username, address = "localhost";
    ArrayList<String> users = new ArrayList();
    int port = 2000;
    Boolean isConnected = false;
    
    Socket sock;
    BufferedReader reader;
    PrintWriter writer;
    
    public Rozgrywka() {
        initComponents();
    }
    
    public Rozgrywka(Zalogowano x) {
        initComponents();
        username=x.nick;
        address=x.ip;
        port=x.port;
        show_nick.setText(username);
        ta_chat.setEditable(false);
        ConnectFirst();
        //Gra gra = new Gra(p_game);
    }
    
    public void ConnectFirst(){
        try 
        {
            sock = new Socket(address, port);
            InputStreamReader streamreader = new InputStreamReader(sock.getInputStream());
            reader = new BufferedReader(streamreader);
            writer = new PrintWriter(sock.getOutputStream());
            isConnected = true;
        } 
        catch (Exception ex) 
        {
            ta_chat.append("Nie można ustalić połączenia! Spróbuj ponownie. \n");
        }
        ListenThread();
    }
    
    public void ListenThread() 
    {
        Thread IncomingReader = new Thread(new IncomingReader());
        IncomingReader.start();
    }
    
    public void userAdd(String data) 
    {
        users.add(data);
    }
    
    public void userRemove(String data) 
    {
        ta_chat.append(data + " jest offline.\n");
    }
    
    public void writeUsers() 
    {
        String[] tempList = new String[(users.size())];
        users.toArray(tempList);
        for (String token:tempList) 
        {
            //users.append(token + "\n");
        }
    }
    
    public void sendDisconnect() 
    {
        String bye = (username + ": :Disconnect");
        try
        {
            writer.println(bye);
            writer.flush();
        } catch (Exception e) 
        {
            ta_chat.append("Could not send Disconnect message.\n");
        }
    }
    
    public void Disconnect() 
    {
        try 
        {
            ta_chat.append("Rozłączono.\n");
            sock.close();
        } catch(Exception ex) {
            ta_chat.append("Nie udało się rozłączyć. \n");
        }
        isConnected = false;
    }

    public class IncomingReader implements Runnable
    {
        @Override
        public void run()
        {
            String[] data;
            String stream, done = "Done", connect = "Connect", disconnect = "Disconnect", chat = "Chat";

            try 
            {
                while ((stream = reader.readLine()) != null) 
                {
                     data = stream.split(":");

                     if (data[2].equals(chat)) 
                     {
                        ta_chat.append(data[0] + ": " + data[1] + "\n");
                        ta_chat.setCaretPosition(ta_chat.getDocument().getLength());
                     } 
                     else if (data[2].equals(connect))
                     {
                        ta_chat.removeAll();
                        userAdd(data[0]);
                     } 
                     else if (data[2].equals(disconnect)) 
                     {
                        userRemove(data[0]);
                     } 
                     else if (data[2].equals(done)) 
                     {
                        //users.setText("");
                        writeUsers();
                        users.clear();
                     }
                }
           }catch(Exception ex) { }
        }
    }

    //--------------------------//
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        b_connect = new javax.swing.JButton();
        b_disconnect = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        ta_chat = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        show_nick = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tf_chat = new javax.swing.JTextArea();
        p_game = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Rozgrywka");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setName("client"); // NOI18N
        setResizable(false);

        b_connect.setText("Połącz");
        b_connect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_connectActionPerformed(evt);
            }
        });

        b_disconnect.setText("Wyloguj");
        b_disconnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_disconnectActionPerformed(evt);
            }
        });

        ta_chat.setColumns(20);
        ta_chat.setRows(5);
        jScrollPane1.setViewportView(ta_chat);

        jLabel1.setText("Twój nick:");

        show_nick.setText("NotAvailable");

        tf_chat.setColumns(20);
        tf_chat.setLineWrap(true);
        tf_chat.setRows(5);
        tf_chat.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tf_chatFocusGained(evt);
            }
        });
        tf_chat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tf_chatKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tf_chatKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tf_chatKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(tf_chat);

        javax.swing.GroupLayout p_gameLayout = new javax.swing.GroupLayout(p_game);
        p_game.setLayout(p_gameLayout);
        p_gameLayout.setHorizontalGroup(
            p_gameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 720, Short.MAX_VALUE)
        );
        p_gameLayout.setVerticalGroup(
            p_gameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 720, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(p_game, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 71, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(56, 56, 56))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(b_disconnect, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(103, 103, 103))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(b_connect, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(show_nick)
                        .addGap(83, 83, 83))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(show_nick))
                        .addGap(34, 34, 34)
                        .addComponent(b_connect)
                        .addGap(61, 61, 61)
                        .addComponent(b_disconnect, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(54, 54, 54)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(p_game, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void b_connectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_connectActionPerformed
        if (isConnected == false)
        {
            try 
            {
                sock = new Socket(address, port);
                InputStreamReader streamreader = new InputStreamReader(sock.getInputStream());
                reader = new BufferedReader(streamreader);
                writer = new PrintWriter(sock.getOutputStream());
                writer.println(username + ": połączył się.:Connect");
                writer.flush(); 
                isConnected = true; 
            } 
            catch (Exception ex) 
            {
                ta_chat.append("Nie można ustalić połączenia! Spróbuj ponownie.\n");
            }
            ListenThread();
        }
        else if (isConnected == true) 
        {
            ta_chat.append("Już jesteś połączony. \n");
        }
    }//GEN-LAST:event_b_connectActionPerformed

    private void b_disconnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_disconnectActionPerformed
        SimpleDateFormat simpleDateHere = new SimpleDateFormat("kk:mm:ss");
        String time = simpleDateHere.format(new Date());
        SimpleDateFormat date = new SimpleDateFormat("yy-MM-dd");
        String data = date.format(new Date());
        
        RandomAccessFile files = null;
        try {
            files = new RandomAccessFile(data+".txt", "rw");
        } catch (FileNotFoundException e) {
          System.out.println("BŁĄD PRZY OTWIERANIU PLIKU!");
          System.exit(1);
        }
        
        try {
            files.seek(files.length());
            String x = "\n"+ta_chat.getText();
            //System.out.println(a);
            //files.seek(files.length());
            //files.writeBytes(enter);
            //files.writeBytes(enter);
            files.writeBytes(x);  
        } catch (IOException e1) {
            System.out.println("BŁĄD ODCZYTU Z PLIKU!");
            System.exit(2);
        }
     
        try {
            files.close();
        } catch (IOException e) {
            System.out.println("BŁĄD PRZY ZAMYKANIU PLIKU!");
            System.exit(3); 
        }
        
        sendDisconnect();
        Disconnect();
        Main x = new Main();
        try {
            Thread.sleep(1000);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        x.setVisible(true);
        dispose();
    }//GEN-LAST:event_b_disconnectActionPerformed

    private void tf_chatKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tf_chatKeyTyped

    }//GEN-LAST:event_tf_chatKeyTyped

    private void tf_chatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tf_chatKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            evt.consume();
            String nothing = "";
            if ((tf_chat.getText()).equals(nothing)) {
                tf_chat.setText("");
                tf_chat.requestFocus();
            } 
            else {
                try {
                   writer.println(username + ":" + tf_chat.getText() + ":" + "Chat");
                   writer.flush(); // flushes the buffer
                } catch (Exception ex) {
                    ta_chat.append("Wiadomość nie została wysłana. \n");
                }
                tf_chat.setText("");
                tf_chat.requestFocus();
            }
            tf_chat.setText("");
            tf_chat.requestFocus();
        }
    }//GEN-LAST:event_tf_chatKeyPressed

    private void tf_chatFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tf_chatFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_tf_chatFocusGained

    private void tf_chatKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tf_chatKeyReleased

    }//GEN-LAST:event_tf_chatKeyReleased

    
    public static void main(String args[]) 
    {
        java.awt.EventQueue.invokeLater(new Runnable() 
        {
            @Override
            public void run() 
            {
                new Rozgrywka().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_connect;
    private javax.swing.JButton b_disconnect;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel p_game;
    private javax.swing.JLabel show_nick;
    private javax.swing.JTextArea ta_chat;
    private javax.swing.JTextArea tf_chat;
    // End of variables declaration//GEN-END:variables
}
