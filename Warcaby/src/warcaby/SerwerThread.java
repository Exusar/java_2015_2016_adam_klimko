package warcaby;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Set;
import javax.swing.JTextArea;

public class SerwerThread extends Thread{
    Socket socket;
    ObjectInputStream in;
    ObjectOutputStream out;
    JTextArea ta_chat;
    private Set<SerwerThread> connections; //
    static int playerID = 1;
    Msg x;
    
    SerwerThread(Socket client, JTextArea receive, Set<SerwerThread> set){
        socket = client;
        ta_chat = receive;
        connections = set;
        x = new Msg(2,playerID);
        playerID+=2;
    }
    
    public void sendMsg(Msg msg){
        for(SerwerThread a: connections){ 
            try {
                a.out.writeObject(msg);
                a.out.flush();
            } catch (IOException ex) {
                System.err.println("Blad przy wysylaniu do wszystkich, serwer");
            }
        }
    }
    
    @Override
    public void run(){
        try {
            in = new ObjectInputStream(socket.getInputStream());
            out = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException ex) {
            System.err.println("Blad podczas Input/Output w serwerze");
        }
        sendMsg(x);
        if(playerID>3){
            Msg play = new Msg(1,true);
            sendMsg(play);
        }
        while(true){
            try {
                Msg msg = (Msg) in.readObject();
                sendMsg(msg);
                if (msg.type==6){
                    ta_chat.append("Receive: " + msg.nick + " disconnected. Send to everyone.\n");
                    user_remove();
                }
                else if(msg.type==3){
                    ta_chat.append("Receive message from " + msg.nick + ". Send to everyone.\n");
                }
            } catch (IOException ex) {
                System.err.println("Blad przy odczycie wiadomosci w serwerze");
            } catch (ClassNotFoundException ex) {
                System.err.println("Blad przy odczycie wiadomosci w serwerze");
            }
        }
    }
    
    void user_remove(){
        connections.remove(this);
        this.stop();
    }
}
